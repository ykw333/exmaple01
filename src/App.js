import React from 'react';

class App extends React.Component{
  
  constructor(props){
    super(props);
    console.log("constructor");
  }
  componentDidMount(){
    console.log("component did mount");
  }
  componentDidUpdate(){
    console.log("component did update");
  }
  componentWillUnmount(){
    console.log("component will unmount");
  }
  state = {
    count : 0
  };

  add = () => {
    console.log("add")
    this.setState(current => ({
      count : current.count + 1
    }));
  };

  minus = () => {
    console.log("minus")
    this.setState(current => ({
      count : current.count - 1
    }));
  };



  render(){
    console.log("rendering");
    return (
      <div>
        <h1>Tthis Count : {this.state.count}</h1>
        <button onClick={this.add}>Add</button>
        <button onClick={this.minus}>minus</button>
      </div>
    );
  }
}

export default App;
